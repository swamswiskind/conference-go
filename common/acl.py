import json
from .keys import keys
import requests


def get_location_picture(city, state):
    key = keys.get("PEXELS_API_KEY")
    authorization = {"AUTHORIZATION": key}
    response = requests.get(
        f"https://api.pexels.com/v1/search?query={city}%20{state}&per_page=1",
        headers=authorization,
        timeout=5,
    )
    content = response.content
    picture = json.loads(content)
    picture_dict = picture["photos"][0]["src"]
    picture_url = picture_dict["original"]
    return picture_url


def get_current_weather(city, state):
    key = keys.get("OPEN_WEATHER_API_KEY")
    lat_lon_response = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={key}",
        timeout=5,
    )
    lat_lon = json.loads(lat_lon_response.content)
    latitude = lat_lon[0].get("lat")
    longitude = lat_lon[0].get("lon")

    response = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={key}&units=imperial",
        timeout=5,
    )
    content = response.content
    current_weather = json.loads(content)
    temperature = current_weather.get("main").get("temp")
    description = current_weather.get("weather")[0].get("description")
    weather = {
        "temperature": temperature,
        "description": description,
    }

    return weather if bool(content) else None
