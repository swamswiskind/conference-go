from django.urls import path

from .api_views import api_list_presentations, api_show_presentation, approve, reject


urlpatterns = [
    path(
        "conferences/<int:conference_id>/presentations/",
        api_list_presentations,
        name="api_list_presentations",
    ),
    path(
        "presentations/<int:id>/",
        api_show_presentation,
        name="api_show_presentation",
    ),
    path(
        "presentations/<int:id>/approved",
        approve,
        name="approve",
    ),
    path(
        "presentations/<int:id>/rejected",
        reject,
        name="reject",
    ),
]
